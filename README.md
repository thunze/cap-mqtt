# SAP CAP MQTT Client

This repository is part of a tutorial explaining the development of an SAP CAP application listening and reacting to MQTT messages. Find out more about the background of this application [here](https://blogs.sap.com/2022/03/03/developing-a-cap-application-integrating-mqtt-with-integration-suite/).

## Getting Started

- Install [node.js](https://nodejs.org/).
- Install `@sap/cds-dk` globally using `npm install -g @sap/cds-dk`.
- Configure the application using `/config.js` and the tutorial provided [here](https://www.youtube.com/watch?v=jDlkjcdNfUo).
- Run the application locally using `cds watch`.
